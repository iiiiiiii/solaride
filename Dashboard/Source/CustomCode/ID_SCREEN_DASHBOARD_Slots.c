/*********************************************************************
*                     SEGGER Microcontroller GmbH                    *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 1996 - 2021  SEGGER Microcontroller GmbH                *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************
----------------------------------------------------------------------
File        : ID_SCREEN_DASHBOARD_Slots.c
Purpose     : AppWizard managed file, function content could be changed
---------------------------END-OF-HEADER------------------------------
*/

#include "Application.h"
#include "../Generated/Resource.h"
#include "../Generated/ID_SCREEN_DASHBOARD.h"

/*** Begin of user code area ***/
/*** End of user code area ***/

/*********************************************************************
*
*       Public code
*
**********************************************************************
*/
/*********************************************************************
*
*       cbID_SCREEN_DASHBOARD
*/
void cbID_SCREEN_DASHBOARD(WM_MESSAGE * pMsg) {
  GUI_USE_PARA(pMsg);
}

/*********************************************************************
*
*       ID_SCREEN_00__WM_NOTIFICATION_VALUE_CHANGED__ID_SLIDER_00__APPW_JOB_SETVALUE_0
*/
void ID_SCREEN_00__WM_NOTIFICATION_VALUE_CHANGED__ID_SLIDER_00__APPW_JOB_SETVALUE_0(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_00__WM_NOTIFICATION_VALUE_CHANGED__ID_PROGBAR_TB_00__APPW_JOB_SETVALUE_8
*/
void ID_SCREEN_00__WM_NOTIFICATION_VALUE_CHANGED__ID_PROGBAR_TB_00__APPW_JOB_SETVALUE_8(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_00__WM_NOTIFICATION_VALUE_CHANGED__ID_GAUGE_00__APPW_JOB_SETVALUE
*/
void ID_SCREEN_00__WM_NOTIFICATION_VALUE_CHANGED__ID_GAUGE_00__APPW_JOB_SETVALUE(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_PROGBAR_CHARGE__APPW_JOB_SETVALUE
*/
void ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_PROGBAR_CHARGE__APPW_JOB_SETVALUE(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_TEXT_02__APPW_JOB_SETVALUE
*/
void ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_TEXT_02__APPW_JOB_SETVALUE(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_IMAGE_00__APPW_JOB_SETBITMAP_1
*/
void ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_IMAGE_00__APPW_JOB_SETBITMAP_1(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__ILLEGAL_NOTIFICATION__ID_IMAGE_00__APPW_JOB_SETBITMAP
*/
void ID_SCREEN_DASHBOARD__ILLEGAL_NOTIFICATION__ID_IMAGE_00__APPW_JOB_SETBITMAP(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_IMAGE_00__APPW_JOB_SETBITMAP
*/
void ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_IMAGE_00__APPW_JOB_SETBITMAP(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_IMAGE_00__APPW_JOB_SETBITMAP_0
*/
void ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_IMAGE_00__APPW_JOB_SETBITMAP_0(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__ID_TIMER_ARROW_LEFT__APPW_NOTIFICATION_TIMER__ID_SWITCH_ARROW_LEFT__APPW_JOB_TOGGLE
*/
void ID_SCREEN_DASHBOARD__ID_TIMER_ARROW_LEFT__APPW_NOTIFICATION_TIMER__ID_SWITCH_ARROW_LEFT__APPW_JOB_TOGGLE(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__ID_TIMER_ARROW_RIGHT__APPW_NOTIFICATION_TIMER__ID_SWITCH_ARROW_RIGHT__APPW_JOB_TOGGLE
*/
void ID_SCREEN_DASHBOARD__ID_TIMER_ARROW_RIGHT__APPW_NOTIFICATION_TIMER__ID_SWITCH_ARROW_RIGHT__APPW_JOB_TOGGLE(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_TIMER_ARROW_LEFT__APPW_JOB_START
*/
void ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_TIMER_ARROW_LEFT__APPW_JOB_START(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_TIMER_ARROW_RIGHT__APPW_JOB_START
*/
void ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_TIMER_ARROW_RIGHT__APPW_JOB_START(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_TIMER_ARROW_LEFT__APPW_JOB_STOP_0
*/
void ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_TIMER_ARROW_LEFT__APPW_JOB_STOP_0(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_TIMER_ARROW_RIGHT__APPW_JOB_STOP
*/
void ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_TIMER_ARROW_RIGHT__APPW_JOB_STOP(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_SWITCH_ARROW_LEFT__APPW_JOB_CLEAR
*/
void ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_SWITCH_ARROW_LEFT__APPW_JOB_CLEAR(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_TIMER_ARROW_LEFT__APPW_JOB_STOP
*/
void ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_TIMER_ARROW_LEFT__APPW_JOB_STOP(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_TEXT_DRIVE__APPW_JOB_SETCOLOR_4
*/
void ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_TEXT_DRIVE__APPW_JOB_SETCOLOR_4(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_TEXT_DRIVE__APPW_JOB_SETCOLOR_3
*/
void ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_TEXT_DRIVE__APPW_JOB_SETCOLOR_3(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_TEXT_DRIVE__APPW_JOB_SETCOLOR_2
*/
void ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_TEXT_DRIVE__APPW_JOB_SETCOLOR_2(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_TEXT_DRIVE__APPW_JOB_SETCOLOR_0
*/
void ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_TEXT_DRIVE__APPW_JOB_SETCOLOR_0(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_TEXT_DRIVE__APPW_JOB_SETCOLOR_1
*/
void ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_TEXT_DRIVE__APPW_JOB_SETCOLOR_1(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_TEXT_DRIVE__APPW_JOB_SETCOLOR
*/
void ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_TEXT_DRIVE__APPW_JOB_SETCOLOR(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_00__ID_ROTARY_00__WM_NOTIFICATION_VALUE_CHANGED
*/
void ID_SCREEN_00__ID_ROTARY_00__WM_NOTIFICATION_VALUE_CHANGED(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__ID_SWITCH_CHARGE__WM_NOTIFICATION_SET_1
*/
void ID_SCREEN_DASHBOARD__ID_SWITCH_CHARGE__WM_NOTIFICATION_SET_1(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__ID_SWITCH_CHARGE__WM_NOTIFICATION_SET_0
*/
void ID_SCREEN_DASHBOARD__ID_SWITCH_CHARGE__WM_NOTIFICATION_SET_0(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__ID_SWITCH_CHARGE__WM_NOTIFICATION_SET
*/
void ID_SCREEN_DASHBOARD__ID_SWITCH_CHARGE__WM_NOTIFICATION_SET(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__ID_SWITCH_CHARGE__WM_NOTIFICATION_CLEAR
*/
void ID_SCREEN_DASHBOARD__ID_SWITCH_CHARGE__WM_NOTIFICATION_CLEAR(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__ID_SLIDER_CHARGE__WM_NOTIFICATION_VALUE_CHANGED
*/
void ID_SCREEN_DASHBOARD__ID_SLIDER_CHARGE__WM_NOTIFICATION_VALUE_CHANGED(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__ID_SWITCH_DIRECTIONLEFT__WM_NOTIFICATION_SET
*/
void ID_SCREEN_DASHBOARD__ID_SWITCH_DIRECTIONLEFT__WM_NOTIFICATION_SET(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__ID_SWITCH_DIRECTIONLEFT__WM_NOTIFICATION_SET__ID_SWITCH_DIRECTIONRIGHT__APPW_JOB_CLEAR
*/
void ID_SCREEN_DASHBOARD__ID_SWITCH_DIRECTIONLEFT__WM_NOTIFICATION_SET__ID_SWITCH_DIRECTIONRIGHT__APPW_JOB_CLEAR(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__ID_SWITCH_ARROW_LEFT__WM_NOTIFICATION_SET_0
*/
void ID_SCREEN_DASHBOARD__ID_SWITCH_ARROW_LEFT__WM_NOTIFICATION_SET_0(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__ID_SWITCH_DIRECTIONLEFT__WM_NOTIFICATION_CLEAR
*/
void ID_SCREEN_DASHBOARD__ID_SWITCH_DIRECTIONLEFT__WM_NOTIFICATION_CLEAR(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__ID_SWITCH_DIRECTIONRIGHT__WM_NOTIFICATION_SET_1
*/
void ID_SCREEN_DASHBOARD__ID_SWITCH_DIRECTIONRIGHT__WM_NOTIFICATION_SET_1(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__ID_SWITCH_DIRECTIONRIGHT__WM_NOTIFICATION_SET__ID_SWITCH_DIRECTIONLEFT__APPW_JOB_CLEAR
*/
void ID_SCREEN_DASHBOARD__ID_SWITCH_DIRECTIONRIGHT__WM_NOTIFICATION_SET__ID_SWITCH_DIRECTIONLEFT__APPW_JOB_CLEAR(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__ID_SWITCH_DIRECTIONRIGHT__WM_NOTIFICATION_SET_0
*/
void ID_SCREEN_DASHBOARD__ID_SWITCH_DIRECTIONRIGHT__WM_NOTIFICATION_SET_0(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__ID_SWITCH_DIRECTIONRIGHT__WM_NOTIFICATION_SET
*/
void ID_SCREEN_DASHBOARD__ID_SWITCH_DIRECTIONRIGHT__WM_NOTIFICATION_SET(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__ID_SWITCH_WARNING__WM_NOTIFICATION_SET_1
*/
void ID_SCREEN_DASHBOARD__ID_SWITCH_WARNING__WM_NOTIFICATION_SET_1(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__ID_SWITCH_WARNING__WM_NOTIFICATION_SET__ID_SWITCH_DIRECTIONLEFT__APPW_JOB_CLEAR
*/
void ID_SCREEN_DASHBOARD__ID_SWITCH_WARNING__WM_NOTIFICATION_SET__ID_SWITCH_DIRECTIONLEFT__APPW_JOB_CLEAR(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__ID_SWITCH_WARNING__WM_NOTIFICATION_SET
*/
void ID_SCREEN_DASHBOARD__ID_SWITCH_WARNING__WM_NOTIFICATION_SET(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__ID_SWITCH_WARNING__WM_NOTIFICATION_SET_0
*/
void ID_SCREEN_DASHBOARD__ID_SWITCH_WARNING__WM_NOTIFICATION_SET_0(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__ID_SWITCH_WARNING__WM_NOTIFICATION_SET_3
*/
void ID_SCREEN_DASHBOARD__ID_SWITCH_WARNING__WM_NOTIFICATION_SET_3(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__ID_SWITCH_WARNING__WM_NOTIFICATION_SET_2
*/
void ID_SCREEN_DASHBOARD__ID_SWITCH_WARNING__WM_NOTIFICATION_SET_2(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__ID_SWITCH_DRIVE__WM_NOTIFICATION_SET
*/
void ID_SCREEN_DASHBOARD__ID_SWITCH_DRIVE__WM_NOTIFICATION_SET(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__ID_SWITCH_DRIVE__WM_NOTIFICATION_SET__ID_SWITCH_NEUTRAL__APPW_JOB_CLEAR
*/
void ID_SCREEN_DASHBOARD__ID_SWITCH_DRIVE__WM_NOTIFICATION_SET__ID_SWITCH_NEUTRAL__APPW_JOB_CLEAR(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__ID_SWITCH_DRIVE__WM_NOTIFICATION_SET__ID_SWITCH_REVERSE__APPW_JOB_CLEAR
*/
void ID_SCREEN_DASHBOARD__ID_SWITCH_DRIVE__WM_NOTIFICATION_SET__ID_SWITCH_REVERSE__APPW_JOB_CLEAR(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__ID_SWITCH_DRIVE__WM_NOTIFICATION_CLEAR
*/
void ID_SCREEN_DASHBOARD__ID_SWITCH_DRIVE__WM_NOTIFICATION_CLEAR(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__ID_SWITCH_NEUTRAL__WM_NOTIFICATION_SET
*/
void ID_SCREEN_DASHBOARD__ID_SWITCH_NEUTRAL__WM_NOTIFICATION_SET(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__ID_SWITCH_NEUTRAL__WM_NOTIFICATION_SET__ID_SWITCH_DRIVE__APPW_JOB_CLEAR_0
*/
void ID_SCREEN_DASHBOARD__ID_SWITCH_NEUTRAL__WM_NOTIFICATION_SET__ID_SWITCH_DRIVE__APPW_JOB_CLEAR_0(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__ID_SWITCH_NEUTRAL__WM_NOTIFICATION_SET__ID_SWITCH_REVERSE__APPW_JOB_CLEAR
*/
void ID_SCREEN_DASHBOARD__ID_SWITCH_NEUTRAL__WM_NOTIFICATION_SET__ID_SWITCH_REVERSE__APPW_JOB_CLEAR(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__ID_SWITCH_NEUTRAL__WM_NOTIFICATION_SET__ID_SWITCH_DRIVE__APPW_JOB_CLEAR
*/
void ID_SCREEN_DASHBOARD__ID_SWITCH_NEUTRAL__WM_NOTIFICATION_SET__ID_SWITCH_DRIVE__APPW_JOB_CLEAR(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__ID_SWITCH_REVERSE__WM_NOTIFICATION_SET_2
*/
void ID_SCREEN_DASHBOARD__ID_SWITCH_REVERSE__WM_NOTIFICATION_SET_2(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__ID_SWITCH_REVERSE__WM_NOTIFICATION_SET_1
*/
void ID_SCREEN_DASHBOARD__ID_SWITCH_REVERSE__WM_NOTIFICATION_SET_1(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__ID_SWITCH_REVERSE__WM_NOTIFICATION_SET_0
*/
void ID_SCREEN_DASHBOARD__ID_SWITCH_REVERSE__WM_NOTIFICATION_SET_0(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*********************************************************************
*
*       ID_SCREEN_DASHBOARD__ID_SWITCH_REVERSE__WM_NOTIFICATION_SET
*/
void ID_SCREEN_DASHBOARD__ID_SWITCH_REVERSE__WM_NOTIFICATION_SET(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult) {
  GUI_USE_PARA(pAction);
  GUI_USE_PARA(hScreen);
  GUI_USE_PARA(pMsg);
  GUI_USE_PARA(pResult);
}

/*************************** End of file ****************************/
