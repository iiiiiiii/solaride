/*********************************************************************
*                     SEGGER Microcontroller GmbH                    *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 1996 - 2021  SEGGER Microcontroller GmbH                *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************
----------------------------------------------------------------------
File        : ID_SCREEN_LOGIN.c
Purpose     : Generated file do NOT edit!
---------------------------END-OF-HEADER------------------------------
*/

#include "Resource.h"
#include "ID_SCREEN_LOGIN.h"

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
/*********************************************************************
*
*       _aCreate
*/
static APPW_CREATE_ITEM _aCreate[] = {
  { WM_OBJECT_WINDOW_Create,
    ID_SCREEN_LOGIN, 0,
    { { { DISPOSE_MODE_REL_PARENT, 0, 0, 0 },
        { DISPOSE_MODE_REL_PARENT, 0, 0, 0 },
        { DISPOSE_MODE_REL_PARENT, 0, 0, 0 },
        { DISPOSE_MODE_REL_PARENT, 0, 0, 0 },
      },
      0, 0, 0, 0, 0, 0
    },
  },
  { WM_OBJECT_BOX_Create,
    ID_BOX_BACKGROUND2, ID_SCREEN_LOGIN,
    { { { DISPOSE_MODE_REL_PARENT, 0, 0, 0 },
        { DISPOSE_MODE_REL_PARENT, 0, 0, 0 },
        { DISPOSE_MODE_REL_PARENT, 0, 0, 0 },
        { DISPOSE_MODE_REL_PARENT, 0, 0, 0 },
      },
      0, 0, 0, 0, 0, 0
    },
  },
  { WM_OBJECT_TEXT_Create,
    ID_TEXT_LOGIN, ID_SCREEN_LOGIN,
    { { { DISPOSE_MODE_REL_PARENT, 0, 0, 0 },
        { DISPOSE_MODE_REL_PARENT, 0, 0, 0 },
        { DISPOSE_MODE_NULL, 0, 0, 0 },
        { DISPOSE_MODE_NULL, 0, 0, 0 },
      },
      480, 96, 0, 0, 0, 0
    },
  },
  { WM_OBJECT_WINDOW_Create,
    ID_WINDOW_ANIMATIONFRAME, ID_SCREEN_LOGIN,
    { { { DISPOSE_MODE_REL_PARENT, 81, 0, 0 },
        { DISPOSE_MODE_REL_PARENT, 110, 0, 0 },
        { DISPOSE_MODE_REL_PARENT, 81, 0, 0 },
        { DISPOSE_MODE_REL_PARENT, 0, 0, 0 },
      },
      0, 0, 0, 0, 0, 0
    },
  },
  { WM_OBJECT_IMAGE_Create,
    ID_IMAGE_IDCARD, ID_WINDOW_ANIMATIONFRAME,
    { { { DISPOSE_MODE_REL_PARENT, 109, 0, 0 },
        { DISPOSE_MODE_REL_PARENT, 39, 0, 0 },
        { DISPOSE_MODE_NULL, 0, 0, 0 },
        { DISPOSE_MODE_NULL, 0, 0, 0 },
      },
      100, 65, 0, 0, 0, 0
    },
  },
};

/*********************************************************************
*
*       _aSetup
*/
static const APPW_SETUP_ITEM _aSetup[] = {
  { ID_BOX_BACKGROUND2,       APPW_SET_PROP_COLOR,        { ARG_V(0xffe2c9ac) } },
  { ID_TEXT_LOGIN,            APPW_SET_PROP_COLOR,        { ARG_V(GUI_BLACK) } },
  { ID_TEXT_LOGIN,            APPW_SET_PROP_ALIGNTEXT,    { ARG_V(GUI_ALIGN_HCENTER | GUI_ALIGN_VCENTER),
                                                            ARG_V(0),
                                                            ARG_V(0) } },
  { ID_TEXT_LOGIN,            APPW_SET_PROP_TEXTID,       { ARG_V(ID_RTEXT_LOGIN) } },
  { ID_TEXT_LOGIN,            APPW_SET_PROP_FONT,         { ARG_VP(0, acNotoSans_40_Normal_EXT_AA4) } },
  { ID_IMAGE_IDCARD,          APPW_SET_PROP_TILE,         { ARG_V(0) } },
  { ID_IMAGE_IDCARD,          APPW_SET_PROP_SBITMAP,      { ARG_VP(0, acsolaride_icon_idcard_100x65),
                                                            ARG_V(797), } },
};

/*********************************************************************
*
*       _aAction
*/
static const APPW_ACTION_ITEM _aAction[] = {
  { ID_SCREEN_LOGIN,          APPW_NOTIFICATION_CREATE,         0,                        APPW_JOB_ANIMCREATE,     ID_SCREEN_LOGIN__APPW_NOTIFICATION_CREATE,
    { ARG_P(&ID_ANIM_IDCARD_Data),
    }, 0, NULL
  },
};

/*********************************************************************
*
*       Public data
*
**********************************************************************
*/
/*********************************************************************
*
*       ID_SCREEN_LOGIN_RootInfo
*/
APPW_ROOT_INFO ID_SCREEN_LOGIN_RootInfo = {
  ID_SCREEN_LOGIN,
  _aCreate, GUI_COUNTOF(_aCreate),
  _aSetup,  GUI_COUNTOF(_aSetup),
  _aAction, GUI_COUNTOF(_aAction),
  cbID_SCREEN_LOGIN,
  0
};

/*************************** End of file ****************************/
