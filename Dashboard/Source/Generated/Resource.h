/*********************************************************************
*                     SEGGER Microcontroller GmbH                    *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 1996 - 2021  SEGGER Microcontroller GmbH                *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************
----------------------------------------------------------------------
File        : Resource.h
Purpose     : Generated file do NOT edit!
---------------------------END-OF-HEADER------------------------------
*/

#ifndef RESOURCE_H
#define RESOURCE_H

#include "AppWizard.h"

/*********************************************************************
*
*       Text
*/
#define ID_RTEXT_TIME 0
#define ID_RTEXT_SPEEDUNITS 1
#define ID_RTEXT_CHARGEUNIT 2
#define ID_RTEXT_DRIVE 3
#define ID_RTEXT_NEUTRAL 4
#define ID_RTEXT_REVERSE 5
#define ID_RTEXT_PARK 6
#define ID_RTEXT_LOGIN 7
#define ID_RTEXT_PLEASEWAIT 8

#define APPW_MANAGE_TEXT APPW_MANAGE_TEXT_EXT
extern const unsigned char acAPPW_Language_0[];

/*********************************************************************
*
*       Fonts
*/
extern const unsigned char acNotoSans_40_Normal_EXT_AA4[];
extern const unsigned char acNettoOT_40_Normal_EXT_AA4[];
extern const unsigned char acNettoOT_16_Normal_EXT_AA4[];
extern const unsigned char acNotoSans_32_Normal_EXT_AA4[];

/*********************************************************************
*
*       Images
*/
extern const unsigned char acsolaride_loop_1437x143[];
extern const unsigned char acsolaride_icon_idcard_100x65[];
extern const unsigned char acSlider_START_6x6_R[];
extern const unsigned char acSlider_END_4x6_R[];
extern const unsigned char acSlider_Thumb_20x20[];
extern const unsigned char acsolaride_progbar_tile_black_1x18[];
extern const unsigned char acsolaride_progbar_tile_empty_1x18[];
extern const unsigned char acDARK_Slider_Shaft_H_Left_Blue_30x16[];
extern const unsigned char acDARK_Slider_Shaft_H_Right_Gray_30x16[];
extern const unsigned char acDARK_Slider_Thumb_Up_22x22[];
extern const unsigned char acDARK_Slider_Thumb_Down_22x22[];
extern const unsigned char acsolaride_icon_empty_28x28[];
extern const unsigned char acDARK_Switch_Body_H_On_55x20[];
extern const unsigned char acDARK_Switch_Body_H_Off_55x20[];
extern const unsigned char acDARK_Switch_Thumb_26x26[];
extern const unsigned char acsolaride_icon_empty_18x32[];
extern const unsigned char acsolaride_icon_arrowleft_32x32[];
extern const unsigned char acsolaride_empty_1x1[];
extern const unsigned char acsolaride_icon_arrowright_32x32[];
extern const unsigned char acsolaride_icon_charging_18x32[];
extern const unsigned char acsolaride_icon_sun_28x28[];

/*********************************************************************
*
*       Variables
*/
#define ID_VAR_SPEED (GUI_ID_USER + 2049)
#define ID_VAR_CHARGE (GUI_ID_USER + 2048)
#define ID_VAR_CHARGING (GUI_ID_USER + 2050)
#define ID_VAR_SOLAR (GUI_ID_USER + 2051)
#define ID_VAR_DISCHARGING (GUI_ID_USER + 2052)
#define ID_VAR_BATTERY (GUI_ID_USER + 2053)
#define ID_VAR_DIRECTIONLEFT (GUI_ID_USER + 2054)
#define ID_VAR_DIRECTIONRIGHT (GUI_ID_USER + 2055)
#define ID_VAR_DIRECTIONSTOP (GUI_ID_USER + 2056)
#define ID_VAR_DRIVE (GUI_ID_USER + 2057)
#define ID_VAR_NEUTRAL (GUI_ID_USER + 2058)
#define ID_VAR_REVERSE (GUI_ID_USER + 2059)
#define ID_VAR_SWITCH (GUI_ID_USER + 2060)
#define ID_VAR_LOOP (GUI_ID_USER + 2061)

/*********************************************************************
*
*       Animations
*/
#define ID_ANIM_IDCARD (GUI_ID_USER + 0)
#define ID_ANIM_LOOP (GUI_ID_USER + 2)

extern const APPW_ANIM_DATA ID_ANIM_IDCARD_Data;
extern const APPW_ANIM_DATA ID_ANIM_LOOP_Data;

/*********************************************************************
*
*       Screens
*/
#define ID_SCREEN_LOGIN (GUI_ID_USER + 4097)
#define ID_SCREEN_WAIT (GUI_ID_USER + 4098)
#define ID_SCREEN_DASHBOARD (GUI_ID_USER + 4096)

extern APPW_ROOT_INFO ID_SCREEN_LOGIN_RootInfo;
extern APPW_ROOT_INFO ID_SCREEN_WAIT_RootInfo;
extern APPW_ROOT_INFO ID_SCREEN_DASHBOARD_RootInfo;

#define APPW_INITIAL_SCREEN &ID_SCREEN_LOGIN_RootInfo

/*********************************************************************
*
*       Project path
*/
#define APPW_PROJECT_PATH "D:/Sync/Dashboard"

#endif  // RESOURCE_H

/*************************** End of file ****************************/
