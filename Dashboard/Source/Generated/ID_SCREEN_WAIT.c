/*********************************************************************
*                     SEGGER Microcontroller GmbH                    *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 1996 - 2021  SEGGER Microcontroller GmbH                *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************
----------------------------------------------------------------------
File        : ID_SCREEN_WAIT.c
Purpose     : Generated file do NOT edit!
---------------------------END-OF-HEADER------------------------------
*/

#include "Resource.h"
#include "ID_SCREEN_WAIT.h"

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
/*********************************************************************
*
*       _aCreate
*/
static APPW_CREATE_ITEM _aCreate[] = {
  { WM_OBJECT_WINDOW_Create,
    ID_SCREEN_WAIT, 0,
    { { { DISPOSE_MODE_REL_PARENT, 0, 0, 0 },
        { DISPOSE_MODE_REL_PARENT, 0, 0, 0 },
        { DISPOSE_MODE_REL_PARENT, 0, 0, 0 },
        { DISPOSE_MODE_REL_PARENT, 0, 0, 0 },
      },
      0, 0, 0, 0, 0, 0
    },
  },
  { WM_OBJECT_BOX_Create,
    ID_BOX_BACKGROUND3, ID_SCREEN_WAIT,
    { { { DISPOSE_MODE_REL_PARENT, -1, 0, 0 },
        { DISPOSE_MODE_REL_PARENT, 0, 0, 0 },
        { DISPOSE_MODE_REL_PARENT, 1, 0, 0 },
        { DISPOSE_MODE_REL_PARENT, 0, 0, 0 },
      },
      0, 0, 0, 0, 0, 0
    },
  },
  { WM_OBJECT_IMAGE_Create,
    ID_IMAGE_LOOP, ID_SCREEN_WAIT,
    { { { DISPOSE_MODE_REL_PARENT, -1, 0, 0 },
        { DISPOSE_MODE_REL_PARENT, 96, 0, 0 },
        { DISPOSE_MODE_NULL, 0, 0, 0 },
        { DISPOSE_MODE_NULL, 0, 0, 0 },
      },
      1437, 143, 0, 0, 0, 0
    },
  },
  { WM_OBJECT_TEXT_Create,
    ID_TEXT_PLEASEWAIT, ID_SCREEN_WAIT,
    { { { DISPOSE_MODE_REL_PARENT, 0, 0, 0 },
        { DISPOSE_MODE_REL_PARENT, 0, 0, 0 },
        { DISPOSE_MODE_NULL, 0, 0, 0 },
        { DISPOSE_MODE_NULL, 0, 0, 0 },
      },
      480, 96, 0, 0, 0, 0
    },
  },
};

/*********************************************************************
*
*       _aSetup
*/
static const APPW_SETUP_ITEM _aSetup[] = {
  { ID_BOX_BACKGROUND3, APPW_SET_PROP_COLOR,        { ARG_V(0xffe2c9ac) } },
  { ID_IMAGE_LOOP,      APPW_SET_PROP_TILE,         { ARG_V(0) } },
  { ID_IMAGE_LOOP,      APPW_SET_PROP_SBITMAP,      { ARG_VP(0, acsolaride_loop_1437x143),
                                                      ARG_V(10742), } },
  { ID_TEXT_PLEASEWAIT, APPW_SET_PROP_COLOR,        { ARG_V(GUI_BLACK) } },
  { ID_TEXT_PLEASEWAIT, APPW_SET_PROP_ALIGNTEXT,    { ARG_V(GUI_ALIGN_HCENTER | GUI_ALIGN_VCENTER),
                                                      ARG_V(0),
                                                      ARG_V(0) } },
  { ID_TEXT_PLEASEWAIT, APPW_SET_PROP_TEXTID,       { ARG_V(ID_RTEXT_PLEASEWAIT) } },
  { ID_TEXT_PLEASEWAIT, APPW_SET_PROP_FONT,         { ARG_VP(0, acNotoSans_40_Normal_EXT_AA4) } },
};

/*********************************************************************
*
*       _aAction
*/
static const APPW_ACTION_ITEM _aAction[] = {
  { ID_SCREEN_WAIT,     APPW_NOTIFICATION_CREATE,         0,                  APPW_JOB_ANIMCREATE,     ID_SCREEN_WAIT__APPW_NOTIFICATION_CREATE,
    { ARG_P(&ID_ANIM_LOOP_Data),
    }, 0, NULL
  },
  { ID_VAR_LOOP,        WM_NOTIFICATION_VALUE_CHANGED,    ID_IMAGE_LOOP,      APPW_JOB_SETX0,          ID_SCREEN_WAIT__WM_NOTIFICATION_VALUE_CHANGED__ID_IMAGE_LOOP__APPW_JOB_SETX0,
    { ARG_V(0),
    }, 0, NULL
  },
};

/*********************************************************************
*
*       Public data
*
**********************************************************************
*/
/*********************************************************************
*
*       ID_SCREEN_WAIT_RootInfo
*/
APPW_ROOT_INFO ID_SCREEN_WAIT_RootInfo = {
  ID_SCREEN_WAIT,
  _aCreate, GUI_COUNTOF(_aCreate),
  _aSetup,  GUI_COUNTOF(_aSetup),
  _aAction, GUI_COUNTOF(_aAction),
  cbID_SCREEN_WAIT,
  0
};

/*************************** End of file ****************************/
