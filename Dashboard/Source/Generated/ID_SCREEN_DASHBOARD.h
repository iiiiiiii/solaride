/*********************************************************************
*                     SEGGER Microcontroller GmbH                    *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 1996 - 2021  SEGGER Microcontroller GmbH                *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************
----------------------------------------------------------------------
File        : ID_SCREEN_DASHBOARD.h
Purpose     : Generated file do NOT edit!
---------------------------END-OF-HEADER------------------------------
*/

#ifndef ID_SCREEN_DASHBOARD_H
#define ID_SCREEN_DASHBOARD_H

#include "AppWizard.h"

/*********************************************************************
*
*       Objects
*/
#define ID_BOX_BACKGROUND        (GUI_ID_USER + 2)
#define ID_BOX_PANEL             (GUI_ID_USER + 17)
#define ID_SLIDER_SETSPEED       (GUI_ID_USER + 10)
#define ID_GAUGE_SPEED           (GUI_ID_USER + 1)
#define ID_TEXT_SPEED            (GUI_ID_USER + 43)
#define ID_PROGBAR_CHARGE        (GUI_ID_USER + 3)
#define ID_TEXT_TIME             (GUI_ID_USER + 4)
#define ID_TEXT_SPEEDUNIT        (GUI_ID_USER + 5)
#define ID_BOX_BATTERYTIP        (GUI_ID_USER + 6)
#define ID_SLIDER_CHARGE         (GUI_ID_USER + 7)
#define ID_IMAGE_SOLAR           (GUI_ID_USER + 8)
#define ID_TEXT_CHARGE           (GUI_ID_USER + 9)
#define ID_TEXT_CHARGEUNIT       (GUI_ID_USER + 11)
#define ID_SWITCH_SOLAR          (GUI_ID_USER + 13)
#define ID_SWITCH_CHARGE         (GUI_ID_USER + 12)
#define ID_IMAGE_CHARGING        (GUI_ID_USER + 14)
#define ID_SWITCH_ARROW_LEFT     (GUI_ID_USER + 18)
#define ID_SWITCH_ARROW_RIGHT    (GUI_ID_USER + 15)
#define ID_TIMER_ARROW_LEFT      (GUI_ID_USER + 16)
#define ID_TIMER_ARROW_RIGHT     (GUI_ID_USER + 21)
#define ID_SWITCH_DIRECTIONLEFT  (GUI_ID_USER + 19)
#define ID_SWITCH_DIRECTIONRIGHT (GUI_ID_USER + 20)
#define ID_SWITCH_WARNING        (GUI_ID_USER + 22)
#define ID_TEXT_DRIVE            (GUI_ID_USER + 23)
#define ID_TEXT_REVERSE          (GUI_ID_USER + 24)
#define ID_TEXT_NEUTRAL          (GUI_ID_USER + 25)
#define ID_SWITCH_DRIVE          (GUI_ID_USER + 26)
#define ID_SWITCH_NEUTRAL        (GUI_ID_USER + 27)
#define ID_SWITCH_REVERSE        (GUI_ID_USER + 28)

/*********************************************************************
*
*       Slots
*/
void ID_SCREEN_00__WM_NOTIFICATION_VALUE_CHANGED__ID_SLIDER_00__APPW_JOB_SETVALUE_0                             (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_00__WM_NOTIFICATION_VALUE_CHANGED__ID_PROGBAR_TB_00__APPW_JOB_SETVALUE_8                         (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_00__WM_NOTIFICATION_VALUE_CHANGED__ID_GAUGE_00__APPW_JOB_SETVALUE                                (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_PROGBAR_CHARGE__APPW_JOB_SETVALUE                   (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_TEXT_02__APPW_JOB_SETVALUE                          (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_IMAGE_00__APPW_JOB_SETBITMAP_1                      (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__ILLEGAL_NOTIFICATION__ID_IMAGE_00__APPW_JOB_SETBITMAP                                 (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_IMAGE_00__APPW_JOB_SETBITMAP                        (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_IMAGE_00__APPW_JOB_SETBITMAP_0                      (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__ID_TIMER_ARROW_LEFT__APPW_NOTIFICATION_TIMER__ID_SWITCH_ARROW_LEFT__APPW_JOB_TOGGLE   (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__ID_TIMER_ARROW_RIGHT__APPW_NOTIFICATION_TIMER__ID_SWITCH_ARROW_RIGHT__APPW_JOB_TOGGLE (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_TIMER_ARROW_LEFT__APPW_JOB_START                    (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_TIMER_ARROW_RIGHT__APPW_JOB_START                   (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_TIMER_ARROW_LEFT__APPW_JOB_STOP_0                   (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_TIMER_ARROW_RIGHT__APPW_JOB_STOP                    (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_SWITCH_ARROW_LEFT__APPW_JOB_CLEAR                   (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_TIMER_ARROW_LEFT__APPW_JOB_STOP                     (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_TEXT_DRIVE__APPW_JOB_SETCOLOR_4                     (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_TEXT_DRIVE__APPW_JOB_SETCOLOR_3                     (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_TEXT_DRIVE__APPW_JOB_SETCOLOR_2                     (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_TEXT_DRIVE__APPW_JOB_SETCOLOR_0                     (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_TEXT_DRIVE__APPW_JOB_SETCOLOR_1                     (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__WM_NOTIFICATION_VALUE_CHANGED__ID_TEXT_DRIVE__APPW_JOB_SETCOLOR                       (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_00__ID_ROTARY_00__WM_NOTIFICATION_VALUE_CHANGED                                                  (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__ID_SWITCH_CHARGE__WM_NOTIFICATION_SET_1                                               (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__ID_SWITCH_CHARGE__WM_NOTIFICATION_SET_0                                               (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__ID_SWITCH_CHARGE__WM_NOTIFICATION_SET                                                 (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__ID_SWITCH_CHARGE__WM_NOTIFICATION_CLEAR                                               (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__ID_SLIDER_CHARGE__WM_NOTIFICATION_VALUE_CHANGED                                       (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__ID_SWITCH_DIRECTIONLEFT__WM_NOTIFICATION_SET                                          (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__ID_SWITCH_DIRECTIONLEFT__WM_NOTIFICATION_SET__ID_SWITCH_DIRECTIONRIGHT__APPW_JOB_CLEAR(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__ID_SWITCH_ARROW_LEFT__WM_NOTIFICATION_SET_0                                           (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__ID_SWITCH_DIRECTIONLEFT__WM_NOTIFICATION_CLEAR                                        (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__ID_SWITCH_DIRECTIONRIGHT__WM_NOTIFICATION_SET_1                                       (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__ID_SWITCH_DIRECTIONRIGHT__WM_NOTIFICATION_SET__ID_SWITCH_DIRECTIONLEFT__APPW_JOB_CLEAR(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__ID_SWITCH_DIRECTIONRIGHT__WM_NOTIFICATION_SET_0                                       (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__ID_SWITCH_DIRECTIONRIGHT__WM_NOTIFICATION_SET                                         (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__ID_SWITCH_WARNING__WM_NOTIFICATION_SET_1                                              (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__ID_SWITCH_WARNING__WM_NOTIFICATION_SET__ID_SWITCH_DIRECTIONLEFT__APPW_JOB_CLEAR       (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__ID_SWITCH_WARNING__WM_NOTIFICATION_SET                                                (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__ID_SWITCH_WARNING__WM_NOTIFICATION_SET_0                                              (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__ID_SWITCH_WARNING__WM_NOTIFICATION_SET_3                                              (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__ID_SWITCH_WARNING__WM_NOTIFICATION_SET_2                                              (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__ID_SWITCH_DRIVE__WM_NOTIFICATION_SET                                                  (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__ID_SWITCH_DRIVE__WM_NOTIFICATION_SET__ID_SWITCH_NEUTRAL__APPW_JOB_CLEAR               (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__ID_SWITCH_DRIVE__WM_NOTIFICATION_SET__ID_SWITCH_REVERSE__APPW_JOB_CLEAR               (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__ID_SWITCH_DRIVE__WM_NOTIFICATION_CLEAR                                                (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__ID_SWITCH_NEUTRAL__WM_NOTIFICATION_SET                                                (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__ID_SWITCH_NEUTRAL__WM_NOTIFICATION_SET__ID_SWITCH_DRIVE__APPW_JOB_CLEAR_0             (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__ID_SWITCH_NEUTRAL__WM_NOTIFICATION_SET__ID_SWITCH_REVERSE__APPW_JOB_CLEAR             (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__ID_SWITCH_NEUTRAL__WM_NOTIFICATION_SET__ID_SWITCH_DRIVE__APPW_JOB_CLEAR               (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__ID_SWITCH_REVERSE__WM_NOTIFICATION_SET_2                                              (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__ID_SWITCH_REVERSE__WM_NOTIFICATION_SET_1                                              (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__ID_SWITCH_REVERSE__WM_NOTIFICATION_SET_0                                              (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_DASHBOARD__ID_SWITCH_REVERSE__WM_NOTIFICATION_SET                                                (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);

/*********************************************************************
*
*       Callback
*/
void cbID_SCREEN_DASHBOARD(WM_MESSAGE * pMsg);

#endif  // ID_SCREEN_DASHBOARD_H

/*************************** End of file ****************************/
