/*********************************************************************
*                     SEGGER Microcontroller GmbH                    *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 1996 - 2021  SEGGER Microcontroller GmbH                *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************
----------------------------------------------------------------------
File        : ID_SCREEN_WAIT.h
Purpose     : Generated file do NOT edit!
---------------------------END-OF-HEADER------------------------------
*/

#ifndef ID_SCREEN_WAIT_H
#define ID_SCREEN_WAIT_H

#include "AppWizard.h"

/*********************************************************************
*
*       Objects
*/
#define ID_BOX_BACKGROUND3 (GUI_ID_USER + 1)
#define ID_IMAGE_LOOP      (GUI_ID_USER + 4)
#define ID_TEXT_PLEASEWAIT (GUI_ID_USER + 3)

/*********************************************************************
*
*       Slots
*/
void ID_SCREEN_WAIT__APPW_NOTIFICATION_CREATE                                    (APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);
void ID_SCREEN_WAIT__WM_NOTIFICATION_VALUE_CHANGED__ID_IMAGE_LOOP__APPW_JOB_SETX0(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);

/*********************************************************************
*
*       Callback
*/
void cbID_SCREEN_WAIT(WM_MESSAGE * pMsg);

#endif  // ID_SCREEN_WAIT_H

/*************************** End of file ****************************/
