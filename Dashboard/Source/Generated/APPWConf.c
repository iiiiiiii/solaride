/*********************************************************************
*                     SEGGER Microcontroller GmbH                    *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 1996 - 2021  SEGGER Microcontroller GmbH                *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************
----------------------------------------------------------------------
File        : APPWConf.c
Purpose     : Generated file do NOT edit!
---------------------------END-OF-HEADER------------------------------
*/

#include "AppWizard.h"
#include "Resource.h"

/*********************************************************************
*
*       Defines
*
**********************************************************************
*/
#define XSIZE_PHYS 480
#define YSIZE_PHYS 272
#define COLOR_CONVERSION GUICC_M565
#define DISPLAY_DRIVER GUIDRV_WIN32
#define NUM_BUFFERS   2

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
/*********************************************************************
*
*       _apRootList
*/
static APPW_ROOT_INFO * _apRootList[] = {
  &ID_SCREEN_LOGIN_RootInfo,
  &ID_SCREEN_WAIT_RootInfo,
  &ID_SCREEN_DASHBOARD_RootInfo,
};

/*********************************************************************
*
*       _NumScreens
*/
static unsigned _NumScreens = GUI_COUNTOF(_apRootList);

/*********************************************************************
*
*       _aVarList
*/
static APPW_VAR_OBJECT _aVarList[] = {
  { ID_VAR_SPEED, 0, 0, NULL },
  { ID_VAR_CHARGE, 0, 0, NULL },
  { ID_VAR_CHARGING, 0, 0, NULL },
  { ID_VAR_SOLAR, 0, 0, NULL },
  { ID_VAR_DISCHARGING, 0, 0, NULL },
  { ID_VAR_BATTERY, 0, 0, NULL },
  { ID_VAR_DIRECTIONLEFT, 0, 1, NULL },
  { ID_VAR_DIRECTIONRIGHT, 0, 1, NULL },
  { ID_VAR_DIRECTIONSTOP, 0, 1, NULL },
  { ID_VAR_DRIVE, 0, 0, NULL },
  { ID_VAR_NEUTRAL, 0, 0, NULL },
  { ID_VAR_REVERSE, 0, 0, NULL },
  { ID_VAR_SWITCH, 0, 0, NULL },
  { ID_VAR_LOOP, 0, 0, NULL },
};

/*********************************************************************
*
*       _NumVars
*/
static unsigned _NumVars = GUI_COUNTOF(_aVarList);

/*********************************************************************
*
*       _aID_ANIM_IDCARD_Items
*/
static const APPW_ANIM_ITEM _aID_ANIM_IDCARD_Items[] = {
  { ANIM_DECEL,
    { 0x0000, 0x3fff },
    { { ANIM_ATOM_OBJECT_GEO, ANIM_DETAIL_Y0, 0x18010803 },
      { ANIM_ATOM_OBJECT_GEO, ANIM_DETAIL_Y1, 0x18010803 },
      { ANIM_ATOM_OBJECT_GEO, ANIM_DETAIL_Y0, 0x18010803 }
    }
  },
  { ANIM_ACCEL,
    { 0x3fff, 0x7fff },
    { { ANIM_ATOM_OBJECT_GEO, ANIM_DETAIL_Y1, 0x18010803 },
      { ANIM_ATOM_OBJECT_GEO, ANIM_DETAIL_X0, 0x18010803 },
      { ANIM_ATOM_OBJECT_GEO, ANIM_DETAIL_Y0, 0x18010803 }
    }
  }
};

/*********************************************************************
*
*       ID_ANIM_IDCARD_Data
*/
const APPW_ANIM_DATA ID_ANIM_IDCARD_Data = { ID_ANIM_IDCARD, 1000, 1, -1, GUI_COUNTOF(_aID_ANIM_IDCARD_Items), _aID_ANIM_IDCARD_Items };

/*********************************************************************
*
*       _aID_ANIM_LOOP_Items
*/
static const APPW_ANIM_ITEM _aID_ANIM_LOOP_Items[] = {
  { ANIM_LINEAR,
    { 0x0000, 0x7fff },
    { { ANIM_ATOM_CONSTANT, 0, 0x00000000 },
      { ANIM_ATOM_CONSTANT, 0, 0xfffffc43 },
      { ANIM_ATOM_VARIABLE, 0, 0x0000100d }
    }
  }
};

/*********************************************************************
*
*       ID_ANIM_LOOP_Data
*/
const APPW_ANIM_DATA ID_ANIM_LOOP_Data = { ID_ANIM_LOOP, 1000, 1, -1, GUI_COUNTOF(_aID_ANIM_LOOP_Items), _aID_ANIM_LOOP_Items };

/*********************************************************************
*
*       Multibuffering
*/
static U8 _MultibufEnable = 1;

/*********************************************************************
*
*       _apLang
*/
static const char * _apLang[] = {
  (const char *)acAPPW_Language_0,
};

/*********************************************************************
*
*       _TextInit
*/
static const APPW_TEXT_INIT _TextInit = {
  _apLang,
  GUI_COUNTOF(_apLang),
};

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/
/*********************************************************************
*
*       _InitText
*/
static void _InitText(void) {
  APPW_TextInitMem(&_TextInit);
}

/*********************************************************************
*
*       Public code
*
**********************************************************************
*/
/*********************************************************************
*
*       APPW_X_Setup
*/
void APPW_X_Setup(void) {
  APPW_SetpfInitText(_InitText);
  APPW_X_FS_Init();
  APPW_MULTIBUF_Enable(_MultibufEnable);
  APPW_SetData(_apRootList, _NumScreens, _aVarList, _NumVars);
}

/*********************************************************************
*
*       APPW_X_Config
*/
#ifdef WIN32
void APPW_X_Config(void) {
  GUI_MULTIBUF_Config(NUM_BUFFERS);
  GUI_DEVICE_CreateAndLink(DISPLAY_DRIVER, COLOR_CONVERSION, 0, 0);
  if (LCD_GetSwapXY()) {
    LCD_SetSizeEx (0, YSIZE_PHYS, XSIZE_PHYS);
    LCD_SetVSizeEx(0, YSIZE_PHYS, XSIZE_PHYS);
  } else {
    LCD_SetSizeEx (0, XSIZE_PHYS, YSIZE_PHYS);
    LCD_SetVSizeEx(0, XSIZE_PHYS, YSIZE_PHYS);
  }
}
#endif

/*************************** End of file ****************************/
