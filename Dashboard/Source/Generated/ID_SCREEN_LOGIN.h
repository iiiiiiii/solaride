/*********************************************************************
*                     SEGGER Microcontroller GmbH                    *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 1996 - 2021  SEGGER Microcontroller GmbH                *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************
----------------------------------------------------------------------
File        : ID_SCREEN_LOGIN.h
Purpose     : Generated file do NOT edit!
---------------------------END-OF-HEADER------------------------------
*/

#ifndef ID_SCREEN_LOGIN_H
#define ID_SCREEN_LOGIN_H

#include "AppWizard.h"

/*********************************************************************
*
*       Objects
*/
#define ID_BOX_BACKGROUND2       (GUI_ID_USER + 1)
#define ID_TEXT_LOGIN            (GUI_ID_USER + 2)
#define ID_WINDOW_ANIMATIONFRAME (GUI_ID_USER + 4)
#define ID_IMAGE_IDCARD          (GUI_ID_USER + 3)

/*********************************************************************
*
*       Slots
*/
void ID_SCREEN_LOGIN__APPW_NOTIFICATION_CREATE(APPW_ACTION_ITEM * pAction, WM_HWIN hScreen, WM_MESSAGE * pMsg, int * pResult);

/*********************************************************************
*
*       Callback
*/
void cbID_SCREEN_LOGIN(WM_MESSAGE * pMsg);

#endif  // ID_SCREEN_LOGIN_H

/*************************** End of file ****************************/
