/***********************************************************************
*
*  FILE        : RX65n_GUI_base_proj.c
*  DATE        : 2017-12-18
*  DESCRIPTION : Main Program
*
*  NOTE:THIS IS A TYPICAL EXAMPLE.
*
***********************************************************************/
#include "GUI.h"
#include "Pin.h"
#include "DIALOG.h"

void main(void);
void main(void)
{
    R_Pins_Create();

    MainTask();

	while(1)
	{
		GUI_Delay(100);
	}
}
