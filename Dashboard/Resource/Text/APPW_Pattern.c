/*********************************************************************
*                     SEGGER Microcontroller GmbH                    *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 1996 - 2021  SEGGER Microcontroller GmbH                *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************
----------------------------------------------------------------------
File        : APPW_Pattern.c
Purpose     : Conversion into c-array.
              Generated file do NOT edit!
---------------------------END-OF-HEADER------------------------------
*/

#include "AppWizard.h"

const unsigned char acAPPW_Pattern[50UL + 1] = {
  0xFF, 0xFE, 0x20, 0x00, 0x25, 0x00, 0x2E, 0x00, 0x2F, 0x00, 0x30, 0x00, 0x31, 0x00, 0x35, 0x00, 0x3A, 0x00, 0x44, 0x00, 0x48, 0x00, 0x4B, 0x00, 0x4D, 0x00, 0x4E, 0x00, 0x50, 0x00, 0x52, 0x00, 0x61, 0x00, 0x65, 0x00, 0x6C, 0x00, 0x6E, 0x00, 
  0x6F, 0x00, 0x73, 0x00, 0x74, 0x00, 0x75, 0x00, 0x76, 0x00, 
};

/*************************** End of file ****************************/
