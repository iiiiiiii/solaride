/*********************************************************************
*                     SEGGER Microcontroller GmbH                    *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 1996 - 2021  SEGGER Microcontroller GmbH                *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************
----------------------------------------------------------------------
File        : APPW_Language_0.c
Purpose     : Conversion into c-array.
              Generated file do NOT edit!
---------------------------END-OF-HEADER------------------------------
*/

#include "AppWizard.h"

const unsigned char acAPPW_Language_0[56UL + 1] = {
  0x31, 0x30, 0x3A, 0x31, 0x35, 0x0A, 0x4B, 0x4D, 0x2F, 0x48, 0x0A, 0x25, 0x0A, 0x44, 0x0A, 0x4E, 0x0A, 0x52, 0x0A, 0x50, 0x0A, 0x50, 0x61, 0x6C, 0x75, 0x6E, 0x20, 0x74, 0x75, 0x76, 0x61, 0x73, 0x74, 0x61, 0x20, 0x65, 0x6E, 0x6E, 0x61, 0x73, 
  0x74, 0x0A, 0x50, 0x61, 0x6C, 0x75, 0x6E, 0x20, 0x6F, 0x6F, 0x74, 0x61, 0x2E, 0x2E, 0x2E, 0x0A, 
};

/*************************** End of file ****************************/
